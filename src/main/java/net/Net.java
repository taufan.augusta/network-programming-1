package net;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.jsoup.*;

public class Net{

    private File file;
    private String fileDirectory;

    public String SourceViewerLocal() throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileDirectory)), StandardCharsets.UTF_8);
    }

    public String ContentGetter() throws IOException {
        String source = SourceViewerLocal();
        return Jsoup.parse(source).text();
    }

    public String SourceViewerDirect(String url) 
    {
        String content = null;
        URLConnection connection = null;
        
        try {
            connection =  new URL(url).openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            scanner.useDelimiter("\\Z");
            content = scanner.next();
            scanner.close();
        }catch ( Exception ex ) {
            ex.printStackTrace();
        }
        System.out.println(content);
        return content;
    }

    public String ProtocolTester(String url) throws MalformedURLException
    {
        URL u = new URL(url);
        return "The Protocol of " + url + " is " + u.getProtocol();
    }

    public String URI_Splitter(String url) {
        
        URL u = null;
        URI i = null;
        try {            
            u = new URL(url);
            i = u.toURI();
            
        } catch (Exception e) {
            //TODO: handle exception
        }
        return i.toString();

        // return i.toString();
        
    }

    public String URL_Splitter(String url) throws MalformedURLException
    {
        URL u = new URL(url);
        System.out.println("The URL is " + u);
        System.out.println("The scheme is " + u.getProtocol());
        System.out.println("The user info is " + u.getUserInfo());
    
        String host = u.getHost();
        if (host != null) {
          int atSign = host.indexOf('@');
          if (atSign != -1)
            host = host.substring(atSign + 1);
          System.out.println("The host is " + host);
        } else {
          System.out.println("The host is null.");
        }

        System.out.println("The port is " + u.getPort());
        System.out.println("The path is " + u.getPath());
        System.out.println("The ref is " + u.getRef());
        System.out.println("The query string is " + u.getQuery());

        return "The URL is " + u + "\n" + 
            "The scheme is " + u.getProtocol() + "\n" +
            "The host is " + host + "\n" +
            "The port is " + u.getPort() + "\n" +
            "The ref is " + u.getRef() + "\n" +
            "The query string is " + u.getQuery()
            ;

    }

    public void HTML_Fetch(String strUrl)
    {
        try {
            URL url = new URL(strUrl); 
            URLConnection conn  = url.openConnection();
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));

            String inputLine;
            String fileName = "./page.html";
            fileDirectory = fileName;

            file = new File(fileName);

            if(!file.exists())
                file.createNewFile();

            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            while((inputLine = reader.readLine()) != null){
                bufferedWriter.write(inputLine);
            }

            fileWriter.close();
            bufferedWriter.close();
            
        } catch (MalformedURLException e) {
            e.getStackTrace();
        } catch(IOException e){
            e.getStackTrace();
        }
    }





    public String GetFileSourceDirectory()
    {
       return fileDirectory; 
    }



}








